#!/usr/bin/env python
"""
This is the "example" module.

The example module supplies one function, multiply().  For example,

>>> multiply(5, 5)
25
"""


def multiply(x: float, y: float) -> float:
    """
    Return the product of x and y
    :param x:
    :param y:
    :return:

    >>> multiply(3.14, 3.14)
    9.8596
    """
    return x * y


if __name__ == "__main__":
    import doctest
    import sys

    MIN_PYTHON = (3, 7)
    if sys.version_info < MIN_PYTHON:
        sys.exit("Python %s.%s or later is required.\n" % MIN_PYTHON)

    doctest.testmod()  # https://docs.python.org/3.8/library/doctest.html

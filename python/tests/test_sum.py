from unittest import TestCase
from showcase.sum import my_sum


class TestSum(TestCase):
    def test_my_sum(self):
        x = my_sum(10, 3.14)
        self.assertEqual(13.14, x)
